#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <set>
#include <vector>

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "items")
{
    cout << prefix << " : [ ";
    for (const auto& item : container)
        cout << item << " ";
    cout << "]" << endl;
}

int main()
{
    vector<int> vec(35);

    std::random_device rd;
    std::mt19937 rnd_engine{rd()};
    std::uniform_int_distribution<int> uniform_distr{1, 35};

    auto rnd_gen = [&uniform_distr, &rnd_engine] { return uniform_distr(rnd_engine); };

    generate(vec.begin(), vec.end(), rnd_gen);

    vector<int> sorted_vec{vec};
    sort(sorted_vec.begin(), sorted_vec.end());

    multiset<int> mset{vec.begin(), vec.end()};

    print(vec, "vec        : ");
    print(sorted_vec, "sorted_vec : ");
    print(mset, "mset       : ");

    // check if 17 is in sequence
    cout << "vec: 17 is " << (count(vec.begin(), vec.end(), 17) ? "" : "not ") << "found in sequence" << endl;
    cout << "sorted_vec: 17 is " << (binary_search(sorted_vec.begin(), sorted_vec.end(), 17) ? "" : "not ") << "found in sequence" << endl;
    cout << "mset: 17 is " << (mset.count(17) ? "" : "not ") << "found in sequence" << endl;



    cout << "\n\n";
    // print number of occurence of 22
    auto n1 = count(vec.begin(), vec.end(), 22);
    cout << "22 is found " << n1 << " times" << endl;

    auto range_it = equal_range(sorted_vec.begin(), sorted_vec.end(), 22);
    auto n2 = range_it.second - range_it.first;
    cout << "22 is found " << n2 << " times" << endl;;

    cout << "22 is found " << mset.count(22) << " times";



    cout << "\n\n";
    // remove all numbers greater than 15

    vec.erase(remove_if(vec.begin(), vec.end(), [](int x) { return x > 15; }), vec.end());
    print(vec, "vec        : ");

    sorted_vec.erase(upper_bound(sorted_vec.begin(), sorted_vec.end(), 15), sorted_vec.end());
    print(sorted_vec, "sorted_vec : ");

    mset.erase(mset.upper_bound(15), mset.end());
    print(mset, "mset       : ");
}
