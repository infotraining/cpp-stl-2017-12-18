#include <boost/algorithm/string.hpp>
#include <chrono>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <unordered_set>

using namespace std;

int main()
{
    // wszytaj zawartość pliku en.dict ("słownik języka angielskiego")
    // sprawdź poprawość pisowni następującego zdania:
    vector<string> words_list;
    string input_text = "this is an exmple of very badd snetence";
    boost::split(words_list, input_text, boost::is_any_of("\t "));

    auto start = chrono::high_resolution_clock::now();

    ifstream file_in("en.dict");

    if (!file_in)
    {
        cerr << "Error opening a file" << endl;
        return -1;
    }

    // loading a dictionary
    istream_iterator<string> dict_start(file_in);
    istream_iterator<string> dict_end;
    unordered_set<string> dictionary(dict_start, dict_end, 218971);

    // spellcheck
    vector<string> misspelled;

    for(const auto& w : words_list)
    {
        if (not dictionary.count(w))        
            misspelled.push_back(w);       
    }

    auto stop = chrono::high_resolution_clock::now();

    for(const auto& msp : misspelled)    
        cout << "Misspelled: " << msp << endl;

    cout << "\nElapsed time: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << endl;
}
