#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <unordered_map>
#include <set>
#include <string>
#include <chrono>
#include <string_view>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien danego slowa w pliku tekstowym.
    Wyswietl 20 najczęściej występujących slow (w kolejności malejącej).
*/

template <typename It, typename Func>
void for_each_n(It start, size_t n, Func f)
{
    size_t counter = 0;

    for(size_t i = 0; i < n; ++i)
    {
        f(*(start++));
    }
}

int main()
{
    const string file_name = "proust.txt";

    ifstream fin(file_name);

    if (!fin)
        throw runtime_error("File "s + file_name + " can't be opened");

    vector<string> words;

    string word;
    while(fin >> word)
    {
        boost::to_lower(word);
        boost::trim_if(word, boost::is_any_of("\t'?!,.;:()-/\"\\_*"));
        words.push_back(word);
    }

    cout << "Number of words in a book: " << words.size() << endl;

    auto start = chrono::high_resolution_clock::now();

    unordered_map<string, size_t> word_counter(30727);

    for(const auto& word : words)
        word_counter[word]++;

    using concordance_pair = tuple<size_t, string_view>;
    set<concordance_pair, greater<concordance_pair>> concordance;

    for(const auto& kv : word_counter)
        concordance.insert(concordance_pair{kv.second, kv.first});

    auto end = chrono::high_resolution_clock::now();

    auto time_elapsed = chrono::duration_cast<chrono::milliseconds>(end - start);

    cout << "Time elapsed: " << time_elapsed.count() << endl;

    for_each_n(concordance.begin(), 20, [](const auto& item) { cout << get<1>(item) << " - " << get<0>(item) << endl; });
}
