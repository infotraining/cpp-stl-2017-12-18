#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <iterator>

#include "catch.hpp"

using namespace std;
using namespace Catch::Matchers;

class RangeIterator
{
    int value_;

public:
    using value_type = int;
    using iterator_category = input_iterator_tag;

    explicit RangeIterator(int value)
        : value_{value}
    {
    }

    // ++it
    RangeIterator& operator++()
    {
        ++value_;
        return *this;
    }

    // it++
    RangeIterator operator++(int)
    {
        RangeIterator old_value{*this};
        ++value_;

        return old_value;
    }

    bool operator!=(const RangeIterator& other) const
    {
        return value_ != other.value_;
    }

    bool operator==(const RangeIterator& other) const
    {
        return value_ == other.value_;
    }

    // *it
    int operator*() const
    {
        return value_;
    }
};

class Range
{
    int start_;
    int end_;

public:
    Range(int start, int end)
        : start_{start}
        , end_{end}
    {
    }

    RangeIterator begin() const
    {
        return RangeIterator{start_};
    }

    RangeIterator end() const
    {
        return RangeIterator{end_};
    }
};

TEST_CASE("Range")
{
    vector<int> data;

        SECTION("empty range")
        {
            for(const auto& item : Range{1, 1})
            {
                data.push_back(item);
            }

            REQUIRE(data.empty());
        }

        SECTION("many items")
        {
            for(const auto& item : Range{1, 10})
            {
                data.push_back(item);
            }

    //        auto range{1, 10};
    //        for(auto it = range.begin(); it != range.end(); ++it)
    //            data.push_back(*it);

            REQUIRE_THAT(data, Equals(vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9}));
        }
}

TEST_CASE("RangeIterator")
{
    SECTION("pre increment")
    {
        RangeIterator rg_it{1};

        ++rg_it;

        REQUIRE(*rg_it == 2);

        ++++rg_it;

        REQUIRE(*rg_it == 4);
    }

    SECTION("post increment")
    {
        RangeIterator rg_it{1};

        auto result = rg_it++;

        REQUIRE(*result == 1);
        REQUIRE(*rg_it == 2);
    }

    SECTION("not equal operator")
    {
        RangeIterator it1{1};
        RangeIterator it2{2};

        REQUIRE(it1 != it2);
        REQUIRE_FALSE(it1 != it1);
    }

    SECTION("equal operator")
    {
        RangeIterator it1{1};
        RangeIterator it2{2};

        REQUIRE_FALSE(it1 == it2);
        REQUIRE(it1 == it1);
    }
}

template <typename InpIt1, typename InpIt2>
auto zip(InpIt1 start1, InpIt1 end1, InpIt2 start2)
{
    using T1 = decay_t<decltype(*start1)>;
    using T2 = typename iterator_traits<InpIt2>::value_type;
    using ZippedPair = std::tuple<T1, T2>;

    vector<ZippedPair> result;

    while(start1 != end1)
    {
        result.emplace_back(*start1, *start2);
        ++start1;
        ++start2;
    }

    return result;
}


TEST_CASE("zip")
{
    vector<int> vec = { 1, 2, 3 };
    string tab[] = { "one", "two", "three" };

    auto zipped = zip(vec.begin(), vec.end(), begin(tab));

    REQUIRE(zipped[0] == make_tuple(1, "one"));
}
