#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <bitset>

#include "catch.hpp"

using namespace std;

TEST_CASE("bitset")
{
    SECTION("constructors")
    {
        bitset<16> bs(8);

        cout << "bs: " << bs << endl;

        bitset<16> bs2("10101");

        cout << "bs2: " << bs2 << endl;

        REQUIRE_THROWS_AS([] { bitset<16> bs3("01012"); }(), std::invalid_argument);
    }

    SECTION("fliping")
    {
        bitset<16> bs(8);

        bs[5].flip();
        bs.flip();

        cout << "bs: " << bs << endl;
    }

    SECTION("binary operators")
    {
        bitset<16> bs1(8);
        bitset<16> bs2(15);

        cout << "bs1: " << bs1 << endl;
        cout << "bs2: " << bs2 << endl;
        cout << "(bs1 | ~bs2): " << (bs1 | ~bs2) << endl;
    }
}
