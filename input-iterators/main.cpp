#include <iostream>
#include <string>
#include <vector>
#include <iterator>

using namespace std;

int main()
{
    istream_iterator<string> start(cin);
    istream_iterator<string> end;

    vector<string> words(start, end);

    cout << "\nItems:\n";

    copy(words.begin(), words.end(), ostream_iterator<string>(cout, " "));
    cout << endl;
}
