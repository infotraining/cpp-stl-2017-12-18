#define CATCH_CONFIG_MAIN

#include <algorithm>
#include <iostream>
#include <numeric>
#include <random>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "data")
{
    cout << prefix << ": [ ";
    for (const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}

TEST_CASE("algorithms")
{
    vector<int> vec(20);
    iota(vec.begin(), vec.end(), 1);

    print(vec, "vec");

    mt19937_64 rnd_gen{665};

    shuffle(vec.begin(), vec.end(), rnd_gen);
    print(vec, "vec shuffled");

    int sum{};
    for_each(vec.begin(), vec.end(), [&sum](int x) { sum += x; });

    cout << "Sum: " << sum << endl;

    SECTION("counting")
    {
        auto result = count_if(vec.begin(), vec.end(), [](int x) { return x > 5; });

        cout << "Count items > 5: " << result << endl;
    }

    SECTION("all_of; any_of; none_of")
    {
        REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x > 0; }));
    }

    SECTION("min_element, max_element")
    {
        auto it1 = max_element(vec.begin(), vec.end());

        REQUIRE(*it1 == 20);
    }

    SECTION("finding")
    {
        auto where = find(vec.begin(), vec.end(), 19);

        if (where != vec.end())
            cout << "19 is found: " << *where << endl;
    }

    SECTION("search")
    {
        vector<int> sample1 = {3, 7, 2};

        auto where = search(vec.begin(), vec.end(), sample1.begin(), sample1.end());

        if (where != vec.end())
            cout << "Sample1 is found" << endl;
        else
            cout << "Sample1 is not found" << endl;
    }

    SECTION("searching in sorted containers")
    {
        vec.insert(vec.begin(), {34, 2, 53, 2, 2, 2, 45, 5, 5, 5});

        sort(vec.begin(), vec.end());

        REQUIRE(binary_search(vec.begin(), vec.end(), 13));

        auto start = lower_bound(vec.begin(), vec.end(), 5);
        auto end   = upper_bound(vec.begin(), vec.end(), 5);

        for (auto it = start; it != end; ++it)
            cout << *it << " ";
        cout << endl;

        cout << "Count of 5: " << (end - start) << endl;
    }

    SECTION("fill")
    {
        vector<int> vec(10);

        fill(vec.begin(), vec.end(), -1);
    }

    SECTION("generate")
    {
        int seed        = 665;
        auto custom_gen = [seed]() mutable { return ++seed; };

        generate_n(back_inserter(vec), 10, custom_gen);

        print(vec, "vec after generate_n");
    }

    SECTION("remove")
    {
        cout << "\n\n";
        print(vec, "vec");

        auto new_end = remove_if(vec.begin(), vec.end(), [](int x) { return x > 15; });
        vec.erase(new_end, vec.end());

        print(vec, "vec after remove");
    }

    SECTION("transform")
    {
        SECTION("two ranges")
        {
            vector<int> doubled;

            transform(vec.begin(), vec.end(), vec.begin(), back_inserter(doubled), plus<>{});

            print(doubled, "doubled");
        }

        SECTION("one range")
        {
            vector<string> words(vec.size());

            transform(vec.begin(), vec.end(), words.begin(), [](int x) { return "item" + to_string(x); });

            print(words, "words");
        }
    }

    SECTION("merging")
    {
        sort(vec.begin(), vec.end());

        vector<int> data = { 3, 6, 8, 9, 10 , 13};

        vector<int> merged;

        merge(vec.begin(), vec.end(), data.begin(), data.end(), back_inserter(merged));

        print(merged, "merged");

        REQUIRE(is_sorted(merged.begin(), merged.end()));

        SECTION("set operations")
        {
           vector<int> intersection_data;

           set_intersection(vec.begin(), vec.end(), data.begin(), data.end(), back_inserter(intersection_data));

           print(intersection_data, "intersection");
        }
    }

    SECTION("accumulate")
    {
        auto sum = accumulate(vec.begin(), vec.end(), 0LL);

        cout << "sum: " << sum << endl;

        SECTION("fold version")
        {
            vector<string> words = { "one", "two", "three" };

            auto total_length =
                    accumulate(words.begin(), words.end(), 0,
                               [](int total, const string& word) { return total + word.length(); });

            REQUIRE(total_length == 11);
        }
    }
}
