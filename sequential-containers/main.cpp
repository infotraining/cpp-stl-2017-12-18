#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <list>
#include <array>

#include "catch.hpp"

using namespace std;

class Gadget
{
    int id_ = gen_id();
    string name_ = "unknown";
public:
    Gadget() = default;

    Gadget(int id, const string& name)
        : id_{id}, name_{name}
    {
        cout << "Gadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }

    Gadget(const Gadget& source) : id_{source.id_}, name_(source.name_)
    {
        cout << "Gadget(const Gadget& - id:" << id_ << ")" << endl;
    }

    Gadget(Gadget&& source) noexcept : id_{source.id_}, name_{move(source.name_)}
    {
        cout << "Gadget(Gadget&& - id:" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
private:
    static int gen_id()
    {
        static int id_gen;
        return ++id_gen;
    }
};

ostream& operator<<(ostream& out, const Gadget& g)
{
    out << "Gadget(id: " << g.id() << ", name: " << g.name() << ")";
    return out;
}

template <typename Container>
void print(const Container& container, const string& prefix = "data")
{
    cout << prefix << ": [ ";
    for(const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}


TEST_CASE("initializer lists")
{
    auto il = { 1, 2, 3 };

    static_assert(is_same<decltype(il), initializer_list<int>>::value, "Error");

    REQUIRE(il.size() == 3);
    REQUIRE(*(il.begin()) == 1);

    for(auto item : il)
    {
        cout << item << "; ";
    }
    cout << endl;
}

TEST_CASE("vector - constructors")
{
    SECTION("default construction")
    {
        vector<int> vec;

        REQUIRE(vec.size() == 0);
        REQUIRE(vec.capacity() == 0);
        REQUIRE(vec.empty());
    }

    SECTION("contructor with size as argument")
    {
        vector<Gadget> vec(10);

        REQUIRE(vec.size() == 10);
        REQUIRE(vec.capacity() >= 10);

        print(vec, "gadgets");
    }

    SECTION("constructor with size and with value")
    {
        vector<int> vec(10, 665);

        REQUIRE(vec.size() == 10);
        REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x == 665; }));
    }

    SECTION("constructor with initializer list")
    {
        vector<int> vec{10, 665};

        REQUIRE(vec.size() == 2);
        REQUIRE(vec[0] == 10);
        REQUIRE(vec[1] == 665);

        vector<Gadget> gadgets = { Gadget(1, "ipad"), Gadget(2, "mp3 player") };
    }

    SECTION("constructor with iterators")
    {
        int tab[] = { 1, 2, 3, 4, 5 };

        vector<int> vec(begin(tab), end(tab));

        REQUIRE(vec.size() == 5);
    }
}

TEST_CASE("vector - indexing")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 6 };

    REQUIRE(vec[2] == 3);

    SECTION("when index is out of range")
    {
        //vec[10] = 10; // UB

        REQUIRE_THROWS_AS(vec.at(10), std::out_of_range);
    }
}

TEST_CASE("vector - back & front")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 6 };

    auto& item = vec.front(); // int

    item++;

    REQUIRE(vec.front() == 2);
    REQUIRE(item == 2);
}

TEST_CASE("vector - iterators")
{
    vector<int> vec = { 1, 2, 3 };
    const vector<int> cvec = { 1, 2, 3 };

    auto it = vec.begin(); // vector<int>::iterator
    auto it2 = cvec.begin(); // vector<int>::const_iterator
    auto it3 = vec.cbegin(); // vector<int>::const_iterator

    for(auto it = vec.crbegin(); it != vec.crend(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;
}

TEST_CASE("vector - inserting items - primitive types")
{
    vector<int> vec_int;
    vec_int.reserve(100);

    for(int i = 0; i < 64; ++i)
    {
        vec_int.push_back(i);
        cout << "vec_int.size(): " << vec_int.size()
             << "; vec_int.capacity(): " << vec_int.capacity() << endl;
    }

    auto it = vec_int.begin() + 2;

    auto& ref_front = vec_int.front();

    vec_int.insert(it, {-1, -2, -3}); // invalidates ref_front

    cout << "vec_int.size(): " << vec_int.size()
         << "; vec_int.capacity(): " << vec_int.capacity() << endl;

    print(vec_int, "vec_int");

    //cout << ref_front << endl;  // UB
}


TEST_CASE("vector - inserting items - objects")
{
    vector<Gadget> gadgets;

    for(int i = 0; i < 16; ++i)
    {
        gadgets.push_back(Gadget{i+1, "G"s + to_string(i + 1)});
        cout << "gadgets.size(): " << gadgets.size()
             << "; gadgets.capacity(): " << gadgets.capacity() << endl;
    }

    SECTION("emplace")
    {
        gadgets.emplace_back(665, "G665");
    }
}

TEST_CASE("vector - resize")
{
    vector<int> vec = { 1, 2, 3 };

    vec.resize(10);

    REQUIRE(vec.size() == 10);
    REQUIRE(vec.capacity() >= 10);

    cout << "vec.size(): " << vec.size()
         << "; vec.capacity(): " << vec.capacity() << endl;

    print(vec, "vec after resize");

    vec.resize(1);

    cout << "vec.size(): " << vec.size()
         << "; vec.capacity(): " << vec.capacity() << endl;

    print(vec, "vec after resize");

    vec.shrink_to_fit();

    REQUIRE(vec.capacity() == 1);

    cout << "vec.size(): " << vec.size()
         << "; vec.capacity(): " << vec.capacity() << endl;

    print(vec, "vec after shrink_to_fit");
}

TEST_CASE("vector - efficient erase")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 6, 7 };

    auto it = vec.begin();

    SECTION("linear time - O(N)")
    {
        vec.erase(it);

        print(vec, "vec after erase");
    }

    SECTION("constant time - O(1)")
    {
        std::swap(*it, vec.back());
        vec.pop_back();

        print(vec, "vec after erase");
    }
}

namespace Legacy
{
    void copytab(int* tab, int size)
    {
        //...
    }
}

TEST_CASE("vector - is compatibile with raw pointers as iterators")
{
    vector<int> vec = { 1, 2, 3, 4 };

    int* ptrA = &vec[0];
    int* ptrB = vec.data();

    ptrB += 2;

    REQUIRE(*ptrB == 3);

    Legacy::copytab(vec.data(), vec.size());
}

TEST_CASE("vector<bool>")
{
    vector<bool> bits(8);

    print(bits, "bits");

    SECTION("can be indexed")
    {
        bits[0] = true;
        bits[4] = true;

        print(bits, "bits");

        SECTION("can be flipped")
        {
            bits[6].flip();

            bits.flip();

            print(bits, "bits");

            // bool* ptr = &bits[0]; // compilation error

            SECTION("iterating over")
            {
                for(auto&& bit : bits)
                {
                    bit.flip();
                }
            }
        }
    }
}

TEST_CASE("deque")
{
    deque<int> dq = { 1, 2, 3, 4, 5 };

    dq.push_front(7);
    dq.push_front(6);

    print(dq, "dq");

    dq.pop_front();

    print(dq, "dq");
}

TEST_CASE("list")
{
    list<int> lst = { 1 ,6, 34, 2, 345, 65, 767, 665 };

    print(lst, "lst");

    SECTION("push_back & push_front are efficient")
    {
        lst.push_front(999);
        lst.push_back(873);

        print(lst, "lst");
    }

    SECTION("insert")
    {
        auto it = ++(lst.begin());
        auto& ref_item = *it;

        cout << "ref_item: " << ref_item << endl;

        lst.insert(it, 888);

        cout << "ref_item: " << ref_item << endl;
    }

    SECTION("erase")
    {
        auto it = ++(lst.begin());

        it = lst.erase(it);

        //cout << *it << endl; // UB
    }

    SECTION("special ops")
    {
        SECTION("sorting")
        {
            lst.sort();

            print(lst, "lst");
        }

        SECTION("merging")
        {
            lst.sort();

            list<int> lst2 = { 1, 2, 2, 3, 4, 7, 665, 888 };

            lst.merge(lst2);

            print(lst, "lst");
            print(lst2, "lst2");

            SECTION("reversing")
            {
                lst.reverse();
                print(lst, "lst");
            }

            SECTION("removing duplicates")
            {
                lst.unique();

                print(lst, "unique lst");
            }

            SECTION("splicing")
            {
                auto start1 = ++(lst.begin());

                auto start2 = --(lst.end());

                list<int> lst3 = { 1001, 1002, 1003} ;

                auto where = ++(lst3.begin());

                print(lst, "lst before splice");

                lst.splice(where, lst, start1, start2);

                print(lst, "lst after splice");
                print(lst3, "lst3 after splice");
            }
        }
    }
}

TEST_CASE("array")
{
    int tab[10] = { 1, 2, 3, 4 };

    array<int, 10> arr = { { 1, 2, 3, 4 } };

    SECTION("size")
    {
        REQUIRE(arr.size() == 10);
    }

    SECTION("indexing")
    {
        arr[7] = 1;
        REQUIRE_THROWS_AS(arr.at(15), std::out_of_range);
    }

    SECTION("iterators")
    {
        auto it = arr.begin();

        REQUIRE(*(it + 3) == 4);
    }
}
