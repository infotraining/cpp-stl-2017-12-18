#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "data")
{
    cout << prefix << ": [ ";
    for(const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}

TEST_CASE("insert iterators")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 6, 8 };

    vector<int> evens;

    copy_if(vec.begin(), vec.end(), back_inserter(evens), [](int x) { return x % 2 == 0; });

    print(evens, "evens");

    vector<int> data = { 665, 667 };

    copy(data.begin(), data.end(), inserter(vec, vec.begin() + vec.size() / 2));

    print(vec, "vec");
}

TEST_CASE("move iterators")
{
    vector<string> words = { "one", "two", "three" };

    vector<string> short_words;

    copy_if(make_move_iterator(words.begin()), make_move_iterator(words.end()),
            back_inserter(short_words), [](const string& w) { return w.length() == 3; });

    print(words, "words");
    print(short_words, "short_words");
}
