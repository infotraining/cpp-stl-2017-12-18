#define CATCH_CONFIG_MAIN

#include <boost/functional/hash.hpp>
#include <iostream>
#include <random>
#include <set>
#include <string>
#include <unordered_set>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace Cpp17
{
    template <typename... Args>
    size_t get_hash_value(Args&&... t)
    {
        size_t seed = 0;
        (boost::hash_combine(seed, std::forward<Args>(t)), ...);

        return seed;
    }

    template <typename Tpl, size_t... Is>
    auto get_hash_for_tuple_impl(const Tpl& t, std::index_sequence<Is...>)
    {
        return get_hash_value(std::get<Is>(t)...);
    }

    template <typename... Ts>
    auto get_hash_for_tuple(const std::tuple<Ts...>& tpl)
    {
        using Indexes = std::make_index_sequence<sizeof...(Ts)>;
        return get_hash_for_tuple_impl(tpl, Indexes{});
    }

    template <typename T, typename... Ts>
    auto from_il(Ts&&... item)
    {
        vector<T> vec;
        (vec.push_back(std::forward<Ts>(item)), ...);

        return vec;
    }
}

class Gadget
{
    int id_      = gen_id();
    string name_ = "unknown";

    auto tied() const
    {
        return tie(id_, name_);
    }

public:
    Gadget() = default;

    Gadget(int id, const string& name)
        : id_{id}
        , name_{name}
    {
        cout << "Gadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }

    Gadget(const Gadget& source)
        : id_{source.id_}
        , name_(source.name_)
    {
        cout << "Gadget(const Gadget& - id:" << id_ << ")" << endl;
    }

    Gadget(Gadget&& source) noexcept
        : id_{source.id_}
        , name_{move(source.name_)}
    {
        cout << "Gadget(Gadget&& - id:" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    bool operator==(const Gadget& other) const
    {
        return tied() == other.tied();
    }

private:
    static int gen_id()
    {
        static int id_gen;
        return ++id_gen;
    }
};

ostream& operator<<(ostream& out, const Gadget& g)
{
    out << "Gadget(id: " << g.id() << ", name: " << g.name() << ")";
    return out;
}

template <typename Container>
void print(const Container& container, const string& prefix = "data")
{
    cout << prefix << ": [ ";
    for (const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}

template <typename T1, typename T2>
ostream& operator<<(ostream& out, const pair<T1, T2>& p)
{
    out << "{ " << p.first << ", " << p.second << " }";
    return out;
}

TEST_CASE("set")
{
    set<int> set_int = {5, 6, 23, 5, 1, 1, 1, 6, 8, 7};

    print(set_int, "set_int");

    SECTION("inserts")
    {
        auto insert_result = set_int.insert(10);

        REQUIRE(*insert_result.first == 10);
        REQUIRE(insert_result.second == true);

        print(set_int, "set_int");
    }

    SECTION("find")
    {
        auto where = set_int.find(7);

        REQUIRE(where != set_int.end());
        REQUIRE(*where == 7);

        REQUIRE(set_int.count(7) == 1);
    }
}

TEST_CASE("multiset")
{
    multiset<int> mset_int = {4, 245, 12, 2, 1, 1, 4, 6, 7, 7, 5, 99, 7, 10};

    print(mset_int, "mset_int");

    SECTION("lower bound - upper bound")
    {
        auto it1 = mset_int.lower_bound(7);
        auto it2 = mset_int.upper_bound(7);

        REQUIRE(distance(it1, it2) == mset_int.count(7));

        for (auto it = it1; it != it2; ++it)
            cout << *it << " ";
        cout << endl;

        mset_int.erase(it1, it2);

        REQUIRE(mset_int.count(7) == 0);
    }

    SECTION("descending order")
    {
        multiset<int, greater<int>> mset_desc(mset_int.begin(), mset_int.end());

        print(mset_desc, "mset_desc");
    }

    SECTION("custom comparer")
    {
        auto comp_ptrs = [](auto& a, auto& b) { return *a < *b; };

        multiset<const int*, decltype(comp_ptrs)> mset_ptrs(comp_ptrs);

        for (auto& item : mset_int)
        {
            mset_ptrs.insert(&item);
        }

        for (const auto& ptr : mset_ptrs)
            cout << *ptr << " ";
        cout << endl;
    }
}

TEST_CASE("map")
{
    map<int, string> dict = {{1, "one"}, {2, "two"}, {8, "eight"}, {5, "five"}};

    print(dict, "dict");

    SECTION("insert")
    {
        dict.insert(map<int, string>::value_type{4, "four"});
        dict.insert(decltype(dict)::value_type{3, "three"});
        dict.insert(pair<int, string>(9, "nine"));
        // dict.insert(pair(9, "nine")); // C++17
        auto insert_result = dict.insert(make_pair(0, "zero"));

        // auto [where, success] = dict.insert(pair(10, "ten")); // C++17

        REQUIRE(insert_result.second == true);

        print(dict, "dict");
    }

    SECTION("indexing")
    {
        auto& value = dict[8];

        REQUIRE(value == "eight");

        dict[8] = "EIGHT";

        print(dict, "dict");

        SECTION("if key doesn't exist in a map")
        {
            decltype(dict[0]) ref = dict[0];

            cout << "item: " << dict[11] << endl;

            print(dict, "dict");

            SECTION("at() throws")
            {
                REQUIRE_THROWS_AS(dict.at(12), std::out_of_range);
            }
        }
    }

    SECTION("iterating over items")
    {
        for (const auto& kv : dict)
        {
            cout << "Key: " << kv.first << " - Value: " << kv.second << endl;
        }

        // C++17
        //        for(const auto&[key, value] : dict)
        //        {
        //            cout << "Key: " << key << " - Value: " << value << endl;
        //        }
    }
}

template <typename HashContainer>
void print_stats(const HashContainer& container, const string& prefix)
{
    cout << prefix << ": size = " << container.size()
         << "; bucket_count: " << container.bucket_count()
         << "; load_factor: " << container.load_factor()
         << "; max_load_factor: " << container.max_load_factor()
         << endl;
}

TEST_CASE("unordered_set")
{
    unordered_set<int> uset_int;

    print_stats(uset_int, "uset_int");

    uset_int.rehash(256);
    uset_int.max_load_factor(0.7);

    random_device rd{};
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> rnd_distr(0, 1000);

    for (int i = 0; i < 200; ++i)
    {
        uset_int.insert(rnd_distr(rnd_gen));
        print_stats(uset_int, "uset_int");
    }
}

class HashGadget
{
public:
    size_t operator()(const Gadget& g) const
    {
        size_t h_id   = std::hash<int>{}(g.id());
        size_t h_name = std::hash<string>{}(g.name());

        return h_id ^ (h_name << 1);
    }
};

namespace std
{
    template <>
    struct hash<Gadget>
    {
        typedef Gadget argument_type;
        typedef std::size_t result_type;

        result_type operator()(const Gadget& g) const
        {
            size_t seed = 0;
            boost::hash_combine(seed, g.id());
            boost::hash_combine(seed, g.name());

            return seed;
        }
    };
}

TEST_CASE("custom hash")
{
    SECTION("using custom functor")
    {
        unordered_set<Gadget, HashGadget> gadgets;

        gadgets.insert(Gadget{1, "ipad"});
        gadgets.emplace(3, "ipod");
        gadgets.emplace(10, "mp3 player");
    }

    SECTION("using std::hash specialization")
    {
        unordered_set<Gadget> gadgets;

        gadgets.insert(Gadget{1, "ipad"});
        gadgets.emplace(3, "ipod");
        gadgets.emplace(10, "mp3 player");
    }
}

TEST_CASE("hash_combine")
{
    int x      = 10;
    double dx  = 0.1;
    string str = "text";

    size_t seed = 0;
    boost::hash_combine(seed, x);
    boost::hash_combine(seed, dx);
    boost::hash_combine(seed, str);

    SECTION("get_hash_value for many args")
    {
        REQUIRE(seed == Cpp17::get_hash_value(x, dx, str));
    }

    SECTION("get_hash_for_tuple")
    {
        auto tied = tie(x, dx, str);

        REQUIRE(seed == Cpp17::get_hash_for_tuple(tied));
    }
}

TEST_CASE("from_il")
{
    vector<int> vec = Cpp17::from_il<int>(1, 2, 3);

    REQUIRE(vec.size() == 3);

    print(vec, "vec");
}
