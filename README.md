# README #

### Proxy settings ###

* add to .profile

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

### Celero dependencies ###

* https://github.com/DigitalInBlue/Celero
* sudo apt-get install libncurses5-dev


### Ankieta

* https://docs.google.com/forms/d/e/1FAIpQLSeUoR9E6gsgkulYkq7vEqrbz9Yh8zkTazaQDBOx14PoHXq7PA/viewform?hl=pl
